package FortyTwo

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"

	"github.com/joho/godotenv"
)

type tokenC struct {
	Code string `json:"code"`
}

func Login(w http.ResponseWriter, r *http.Request) {
	err := godotenv.Load()
	if err != nil {
		log.Fatal("Error loading .env file")
		return
	}
	clientId := os.Getenv("INTRA_CLIENT_ID")
	secret := os.Getenv("INTRA_SECRET")
	callback := os.Getenv("INTRA_CALLBACK")

	var token tokenC
	b, _ := ioutil.ReadAll(r.Body)
	err = json.Unmarshal(b, &token)
	if err != nil || token.Code == "" {
		return
	}
	values := map[string]string{"grant_type": "authorization_code", "client_id": clientId, "client_secret": secret, "code": token.Code, "redirect_uri": callback}

	jsonValue, _ := json.Marshal(values)
	req, err := http.NewRequest("POST", "https://api.intra.42.fr/oauth/token", bytes.NewBuffer(jsonValue))
	req.Header.Set("Content-Type", "application/json")

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		panic(err)
	}
	defer resp.Body.Close()
	if resp.StatusCode == 401 {
		w.WriteHeader(http.StatusUnauthorized)
	}
	body, _ := ioutil.ReadAll(resp.Body)
	if err != nil {
		fmt.Println(err)
		return
	}
	w.Write(body)
}

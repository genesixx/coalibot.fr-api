package FortyTwo

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"os"
	"sort"

	"github.com/joho/godotenv"
	mgo "gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

type leaderboard struct {
	Login     string  `json:"login"`
	Level     float64 `json:"level"`
	Rentree   string  `json:"rentree"`
	Coalition string  `json:"coalition"`
}

func Leaderboard(w http.ResponseWriter, r *http.Request) {
	var leaderboard []leaderboard
	err := godotenv.Load()
	if err != nil {
		log.Fatal("Error loading .env file")
		return
	}
	session, err := mgo.Dial(os.Getenv("DB_URL"))
	defer session.Close()
	c := session.DB("Coalibot").C("Leaderboard")
	year := r.URL.Query().Get("year")
	if year == "" {
		year = "2018-11"
	}
	err = c.Find(bson.M{"rentree": year}).All(&leaderboard)
	sort.Slice(leaderboard, func(i, j int) bool { return leaderboard[i].Level > leaderboard[j].Level })
	b, err := json.Marshal(leaderboard)
	if err != nil {
		fmt.Println(err)
		return
	}
	w.Header().Set("Content-Type", "application/json")
	w.Write(b)
}
